DESCRIPTION = "The C-Agent is a reference implementation of a \
Keyfactor Orchestrator geared toward use in IoT based solutions."
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://README-LICENSE.txt;md5=86d3f3a95c324c9479bd8986968f4327"

DEPENDS = "curl openssl ca-certificates"

SRC_URI = "git://github.com/Keyfactor/Keyfactor-CAgent;protocol=https"
SRCREV = "eb9fe90418e53f69b8b3b608771dc1a33930c12d"
S = "${WORKDIR}/git"

do_compile() {
    oe_runmake CC="${CC} ${CFLAGS}" -f ${S}/makefile opentest
}

do_install() {
    install -d ${D}/usr/bin
    install -d ${D}/home/root/Keyfactor-CAgent/
    install -d ${D}/home/root/Keyfactor-CAgent/certs

    install -m 0755 ${S}/agent ${D}/usr/bin/
    install -m 0600 ${S}/config.json ${D}/home/root/Keyfactor-CAgent/
    install -m 0600 ${S}/certs/trust.store ${D}/home/root/Keyfactor-CAgent/certs/
}

FILES_${PN} = "/usr/bin/agent \
    /home/root/Keyfactor-CAgent/config.json \
    /home/root/Keyfactor-CAgent/certs\
    /home/root/Keyfactor-CAgent/certs/trust.store \
"
