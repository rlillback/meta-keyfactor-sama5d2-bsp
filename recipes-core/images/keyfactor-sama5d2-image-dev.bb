DESCRIPTION = "keyfactor sama5d2 dev image"
LICENSE="CLOSED"

# .wks file is used to create partitions in image.
# wic* images our used to flash SD cards
# ext4.gz image is used to construct swupdate image.
IMAGE_FSTYPES += "wic wic.gz ext4.gz"

IMAGE_FEATURES += "debug-tweaks ssh-server-openssh"

IMAGE_INSTALL_append = " \
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \
    busybox openssh \
    kernel-image \
    kernel-devicetree \
    kernel-modules \
    i2c-tools \
    usbutils \
    ethtool \
    iperf2 \
    \
    keyfactor-cagent \
    "

export IMAGE_BASENAME = "keyfactor-sama5d2-image-dev"

# This must be at the bottom of this file.
inherit core-image
